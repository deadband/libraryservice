﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json;

namespace LiveChat
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RefreshOnClick(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void Refresh()
        {
            var client = new HttpClient();

            var response = client.GetAsync("http://localhost:9000/api/messages").Result.Content;

            Message[] toPrint;

            var responseResultStream = response.ReadAsStreamAsync().Result;
            
            using (var sr= new StreamReader (responseResultStream))
            {
                toPrint= (Message[])new JsonSerializer().Deserialize(sr, typeof(Message[]));
            }
            allMessages.Text = string.Join(Environment.NewLine, toPrint.Select(p => (p.Username + ": " + p.Content)));
        }

        private void ButtonBase_OnClick2(object sender, RoutedEventArgs e)
        {
            Send();
        }

        private void Send()
        {
            var text = message.Text;
            var client = new HttpClient();
            
            var msg = JsonConvert.SerializeObject(new Message { Username = "user", Content = text });         
                
            var content = new StringContent(msg, Encoding.UTF8, "application/json");
            var result = client.PostAsync("http://localhost:9000/api/Messages", content).Result;
            message.Text = string.Empty;
            Refresh();
        }

        private void FrameworkElement_OnLoaded(object sender, RoutedEventArgs e)
        {
            Refresh();
            message.Focus();
        }

        private void Message_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter )
            {
                Send();
            }
        }


    }
}
