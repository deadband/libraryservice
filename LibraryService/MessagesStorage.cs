﻿using System.Collections.Generic;
using LibraryService.Model;

namespace LibraryService
{
    public class MessagesStorage
    {
        private static readonly List<Message> messagesList = new List<Message>()
            { new Message()
            {
                Content = "Witaj na czacie!", Username = "Server"
            }};

        public static IEnumerable<Message> GetAll()
        {
            return messagesList;
        }

        public static void Add(Message message)
        {
            messagesList.Add(message);
        }
    }
}