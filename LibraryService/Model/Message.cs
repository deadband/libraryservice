﻿namespace LibraryService.Model
{
    public class Message
    {
        public string Username { get; set; }
        public string Content { get; set; }
    }
}
