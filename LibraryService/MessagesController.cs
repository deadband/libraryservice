﻿using System.Collections.Generic;
using System.Web.Http;
using LibraryService.Model;

namespace LibraryService
{
    public class MessagesController : ApiController
    {
        public IEnumerable<Message> GetAll()
        {
            return MessagesStorage.GetAll();
        }

        public void Post(Message message)
        {
            MessagesStorage.Add(message);
        }
    }
}